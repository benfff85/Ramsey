# Ramsey

## Summary

Java implementation of a search algorithm targeting to increase the lower bound of Ramsey Numbers.

This effort is limited to bi-colored symmetric Ramsey Numbers [R(5,5) .. R(x,x)] focusing effort on R(8,8).

Supplemental information about Ramsey Numbers can be found [here](https://en.wikipedia.org/wiki/Ramsey%27s_theorem).

Mutation types are configurable:

* **RANDOM** - A red and blue edge will be selected at random from within the CayleyGraph and will both have their colors flipped. 

* **TARGETED** - An edge will be selected at random from within the selected clique. A second edge of opposite color will be selected randomly from the CayleyGraph and both edges will have their colors flipped. 

* **BALANCED** - The edge with the highest "rank" will be selected from the selected clique. Rank is defined as count of edges matching the clique color attached to the two vertices of a given edge. A second edge of opposite color will be selected randomly from the CayleyGraph and both edges will have their colors flipped. 

* **COMPREHENSIVE** - This will select one red edge and one blue edge which appear in the the clique collection with the greatest frequency and flip their color. It will select randomly from the to X number of most common edges to prevent the same mutation from occurring indefinitely.

## Executing

The project can be built with Maven using the `assembly:assembly install` goals.

Extract the artifact to the desired location

Execute with the following:

```
ramseyHomeDir="/Ramsey"

java -cp ${ramseyHomeDir}/cfg:${ramseyHomeDir}/resources/log4j2.xml:${ramseyHomeDir}/lib/Ramsey-0.0.1-SNAPSHOT.jar com.benajaminleephoto.ramsey.common.Launch
```

## Authors

* **Ben Ferenchak** - *Genesis*