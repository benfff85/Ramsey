package com.benjaminleephoto.ramsey.common.test;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.benjaminleephoto.ramsey.common.Edge;
import static com.benjaminleephoto.ramsey.common.EdgeColor.*;

public class EdgeTest {

	private Edge edge;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		edge = new Edge(null, null, null);
	}

	@After
	public void tearDown() throws Exception {
		edge = null;
	}
	
	@Test
	public void testFlippingBlueEdgeResultsInRedEdge() {
		edge.setColor(BLUE);
		edge.flipColor();
		
		assertEquals(RED, edge.getColor());

	}
	
	@Test
	public void testFlippingRedEdgeResultsInBlueEdge() {
		edge.setColor(RED);
		edge.flipColor();
		
		assertEquals(BLUE, edge.getColor());

	}
	
}
