package com.benjaminleephoto.ramsey.receive;

import java.io.File;

import org.jdom2.Document;
import org.jdom2.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.benjaminleephoto.ramsey.common.ExternalResourceException;

public abstract class FileReceiver implements ReceivingChannel {

    private static final Logger logger = LoggerFactory.getLogger(FileReceiver.class.getName());


    public FileReceiver() {}


    public String receive() throws ExternalResourceException {

        String loaderString;
        File file = getFile();
        loaderString = receiveFromFile(file);

        return loaderString;

    }


    protected abstract File getFile() throws ExternalResourceException;


    private String receiveFromFile(File file) throws ExternalResourceException {

        String output;

        try {

            logger.info("Getting loader string from file: {}", file.getAbsolutePath());

            SAXBuilder builder = new SAXBuilder();
            Document document = builder.build(file);

            output = document.getRootElement().getChildText("EDGES");

            logger.info("Loader string parsed successfully");
            return output;

        } catch (Exception e) {
            logger.error("Error occured while getting loader string from {}.", file.getName());
            throw new ExternalResourceException(e.getMessage());
        }
    }

}
