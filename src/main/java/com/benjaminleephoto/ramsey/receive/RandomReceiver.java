package com.benjaminleephoto.ramsey.receive;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.benjaminleephoto.ramsey.common.ApplicationContext;
import com.benjaminleephoto.ramsey.common.Config;
import com.benjaminleephoto.ramsey.common.ExternalResourceException;

public class RandomReceiver implements ReceivingChannel {

    private static final Logger logger = LoggerFactory.getLogger(RandomReceiver.class.getName());


    public String receive() throws ExternalResourceException {
        return generateRandomLoaderString();
    }


    /**
     * This will Generate a random CayleyGraph where the total number of edges of each color will be
     * equal. This is generally only done once at the beginning of processing after which the
     * initialized graph will simply be mutated.
     */
    private String generateRandomLoaderString() {
        int redCount = Config.EDGE_COUNT / 2;
        StringBuilder loaderString = new StringBuilder();

        logger.info("Generating random loader string");

        for (int i = 0; i < Config.NUM_OF_ELEMENTS; i++) {
            for (int j = i + 1; j < Config.NUM_OF_ELEMENTS; j++) {
                if (redCount > 0 && ApplicationContext.getGenerator().nextBoolean()) {
                    loaderString.append("1");
                    redCount--;
                } else {
                    loaderString.append("0");
                }
            }
        }

        logger.info("Random loader string successfully generated.");

        return loaderString.toString();

    }

}
