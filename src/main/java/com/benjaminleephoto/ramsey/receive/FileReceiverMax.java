package com.benjaminleephoto.ramsey.receive;

import java.io.File;

import com.benjaminleephoto.ramsey.common.Config;

public class FileReceiverMax extends FileReceiver {

    @Override
    protected File getFile() {
        return new File(Config.MAX_FILE_PATH + Config.MAX_FILE_MASK + "MAX" + ".chk");
    }

}
