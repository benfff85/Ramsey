package com.benjaminleephoto.ramsey.receive;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.benjaminleephoto.ramsey.common.Config;
import com.benjaminleephoto.ramsey.common.ExternalResourceException;

public class DatabaseReceiver implements ReceivingChannel {

    private static final Logger logger = LoggerFactory.getLogger(DatabaseReceiver.class.getName());
    private static final String SELECT_GRAPH_QUERY = "SELECT a.edge_data FROM ramsey.graph a WHERE a.vertex_count = ? AND a.subgraph_size = ? AND a.clique_count = (SELECT MIN(b.clique_count) FROM ramsey.graph b WHERE b.vertex_count = ? AND b.subgraph_size = ?)";
    private PreparedStatement selectGraphPS;


    public DatabaseReceiver(Connection connection) throws ExternalResourceException {
        logger.info("Beginning DatabaseReceiver initialization");
        try {
            selectGraphPS = connection.prepareStatement(SELECT_GRAPH_QUERY);
        } catch (SQLException e) {
            String errorMessage = "Failed to initialize DatabaseReceiver";
            logger.error(errorMessage);
            throw new ExternalResourceException(e.getMessage());
        }

        logger.info("DatabaseReceiver initialization complete");

    }


    public String receive() throws ExternalResourceException {

        String loaderString;
        ResultSet selectGraphRS;

        try {
            logger.info("Receiving from database");
            selectGraphPS.setInt(1, Config.NUM_OF_ELEMENTS);
            selectGraphPS.setInt(2, Config.CLIQUE_SIZE);
            selectGraphPS.setInt(3, Config.NUM_OF_ELEMENTS);
            selectGraphPS.setInt(4, Config.CLIQUE_SIZE);
            selectGraphRS = selectGraphPS.executeQuery();
            selectGraphRS.next();
            loaderString = selectGraphRS.getString(1);
            logger.info("Successfully received from database");
            return loaderString;
        } catch (SQLException e) {
            logger.error("Failed to receive from database", e);
            throw new ExternalResourceException(e.getMessage());
        }

    }

}
