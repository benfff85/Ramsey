package com.benjaminleephoto.ramsey.receive;

public enum RECEIVER_TYPE {
    DATABASE("D"), FILE("F"), RANDOM("R");

    private final String text;


    private RECEIVER_TYPE(final String text) {
        this.text = text;
    }


    @Override
    public String toString() {
        return text;
    }
}
