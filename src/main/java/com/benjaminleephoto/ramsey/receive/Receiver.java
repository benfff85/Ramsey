package com.benjaminleephoto.ramsey.receive;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.benjaminleephoto.ramsey.common.Config;
import com.benjaminleephoto.ramsey.common.ExternalResourceException;
import com.benjaminleephoto.ramsey.common.LAUNCH_TYPE;
import com.benjaminleephoto.ramsey.common.RamseyDatabase;

public class Receiver {

    ReceivingChannel receivingChannel;
    private static final Logger logger = LoggerFactory.getLogger(ReceivingChannel.class.getName());


    public Receiver() throws ExternalResourceException {
        logger.info("Starting Receiving Channel Construction");

        if (Config.RECEIVER_METHOD == RECEIVER_TYPE.FILE) {
            receivingChannel = new FileReceiverMax();
        } else if (Config.RECEIVER_METHOD == RECEIVER_TYPE.DATABASE) {
            receivingChannel = new DatabaseReceiver(RamseyDatabase.getConnection());
        } else if (Config.RECEIVER_METHOD == RECEIVER_TYPE.RANDOM) {
            receivingChannel = new RandomReceiver();
        }

        logger.info("Receiving Channel Created Successfully");
    }


    public String receiveGraph() throws ExternalResourceException {
        return receiveWithFailback(receivingChannel);
    }


    public String receiveInitialGraph() throws ExternalResourceException {
        logger.info("Starting Initial Graph Receive");

        ReceivingChannel initialReceivingChannel;

        if (Config.LAUNCH_METHOD == LAUNCH_TYPE.GENERATE_RANDOM) {
            initialReceivingChannel = new RandomReceiver();
        } else if (Config.LAUNCH_METHOD == LAUNCH_TYPE.OPEN_MAX_FROM_DEFAULT_RECEIVER) {
            initialReceivingChannel = receivingChannel;
        } else if (Config.LAUNCH_METHOD == LAUNCH_TYPE.OPEN_MAX_FROM_FILE) {
            initialReceivingChannel = new FileReceiverMax();
        } else if (Config.LAUNCH_METHOD == LAUNCH_TYPE.OPEN_MAX_FROM_DATABASE) {
            initialReceivingChannel = new DatabaseReceiver(RamseyDatabase.getConnection());
        } else if (Config.LAUNCH_METHOD == LAUNCH_TYPE.OPEN_FROM_FILE_INTERACTIVE) {
            initialReceivingChannel = new FileReceiverInteractive();
        } else {
            logger.warn("Something odd happened, unknown LAUNCH_METHOD, assigning random receiver");
            initialReceivingChannel = new RandomReceiver();
        }

        String condensedGraphData = receiveWithFailback(initialReceivingChannel);
        logger.info("Initial Graph Receive Successful");
        return condensedGraphData;

    }


    private String receiveWithFailback(ReceivingChannel localReceivingChannel) throws ExternalResourceException {

        String condensedGraphData = "";

        try {
            condensedGraphData = localReceivingChannel.receive();
        } catch (Exception e) {
            logger.error("Unable to receive from default channel. Will generate a random graph. This may happen on the initial load but should not happen there after.", e);
            ReceivingChannel tempReceivingChannel = new RandomReceiver();
            try {
                condensedGraphData = tempReceivingChannel.receive();
            } catch (Exception e1) {
                logger.error("This should never happen since we are receiving from a random receiver", e1);
                throw new ExternalResourceException(e1.getMessage());
            }
        }
        return condensedGraphData;
    }

}
