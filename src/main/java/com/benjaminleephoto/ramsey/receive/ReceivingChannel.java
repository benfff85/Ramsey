package com.benjaminleephoto.ramsey.receive;

import com.benjaminleephoto.ramsey.common.ExternalResourceException;

public interface ReceivingChannel {

    public String receive() throws ExternalResourceException;

}
