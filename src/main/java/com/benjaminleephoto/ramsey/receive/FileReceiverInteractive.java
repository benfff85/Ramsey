package com.benjaminleephoto.ramsey.receive;

import java.io.File;

import javax.swing.JFileChooser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.benjaminleephoto.ramsey.common.ExternalResourceException;

public class FileReceiverInteractive extends FileReceiver {

    private static final Logger logger = LoggerFactory.getLogger(FileReceiverInteractive.class.getName());


    @Override
    protected File getFile() throws ExternalResourceException {

        JFileChooser chooser = new JFileChooser();
        File file;
        int retval;

        logger.info("Prompting user for input file");
        retval = chooser.showOpenDialog(null);

        if (retval == JFileChooser.APPROVE_OPTION) {
            file = chooser.getSelectedFile();
        } else {
            throw new ExternalResourceException("File not selected");
        }

        return file;

    }
}
