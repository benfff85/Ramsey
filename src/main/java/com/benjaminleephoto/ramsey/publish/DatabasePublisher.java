package com.benjaminleephoto.ramsey.publish;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.benjaminleephoto.ramsey.common.CayleyGraph;
import com.benjaminleephoto.ramsey.common.Config;
import com.benjaminleephoto.ramsey.common.GraphFormatter;
import com.benjaminleephoto.ramsey.common.ExternalResourceException;

public class DatabasePublisher implements PublishingChannel {

    Connection connection;
    PreparedStatement insertGraphPS;

    private static final Logger logger = LoggerFactory.getLogger(DatabasePublisher.class.getName());
    private static final String INSERT_GRAPH_QUERY = "INSERT INTO ramsey.graph (subgraph_size, vertex_count, edge_data, clique_count, identified_date) values (?,?,?,?,DEFAULT)";


    public DatabasePublisher(Connection connection) throws ExternalResourceException {

        logger.info("Beginning DatabasePublisher initialization");

        try {
            insertGraphPS = connection.prepareStatement(INSERT_GRAPH_QUERY);
        } catch (SQLException e) {
            logger.error("Failed to initialize prepared statement in DatabasePublisher", e);
            throw new ExternalResourceException(e.getMessage());
        }

        logger.info("DatabasePublisher initialization complete");

    }


    public void publish(CayleyGraph cayleyGraph) throws ExternalResourceException {

        try {
            logger.info("Publishing to database");
            insertGraphPS.setInt(1, Config.CLIQUE_SIZE);
            insertGraphPS.setInt(2, Config.NUM_OF_ELEMENTS);
            insertGraphPS.setString(3, GraphFormatter.getCondensedGraphData(cayleyGraph));
            insertGraphPS.setInt(4, cayleyGraph.getCliqueCollection().getCliqueCount());
            insertGraphPS.executeUpdate();
            logger.info("Successfully published to database");
        } catch (SQLException e) {
            logger.error("Failed to publish to database", e);
            throw new ExternalResourceException(e.getMessage());
        }

    }

}
