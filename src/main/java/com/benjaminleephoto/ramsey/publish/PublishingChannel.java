package com.benjaminleephoto.ramsey.publish;

import com.benjaminleephoto.ramsey.common.CayleyGraph;
import com.benjaminleephoto.ramsey.common.ExternalResourceException;

public interface PublishingChannel {

    public void publish(CayleyGraph cayleyGraph) throws ExternalResourceException;

}
