package com.benjaminleephoto.ramsey.publish;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.benjaminleephoto.ramsey.common.Config;
import com.benjaminleephoto.ramsey.common.ExternalResourceException;
import com.benjaminleephoto.ramsey.common.RamseyDatabase;

public class PublishingChannelFactory {

    List<PublishingChannel> publishingChannels;
    private static final Logger logger = LoggerFactory.getLogger(PublishingChannelFactory.class.getName());


    public PublishingChannelFactory() throws ExternalResourceException {
        logger.info("Beginning PublisherFactory initialization");

        publishingChannels = Collections.synchronizedList(new ArrayList<PublishingChannel>());

        if (Config.PUBLISH_VIA_FILE) {
            publishingChannels.add(new FilePublisher());
        }

        if (Config.PUBLISH_VIA_DB) {
            publishingChannels.add(new DatabasePublisher(RamseyDatabase.getConnection()));
        }

        logger.info("PublisherFactory initialization successful.");

    }


    public List<PublishingChannel> getPublishingChannels() {
        return publishingChannels;
    }

}
