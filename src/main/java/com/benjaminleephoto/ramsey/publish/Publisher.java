package com.benjaminleephoto.ramsey.publish;

import java.util.List;

import com.benjaminleephoto.ramsey.common.CayleyGraph;
import com.benjaminleephoto.ramsey.common.ExternalResourceException;

public class Publisher {

    List<PublishingChannel> publishingChannels;


    public Publisher() throws ExternalResourceException {

        PublishingChannelFactory publishingChannelFactory = new PublishingChannelFactory();
        publishingChannels = publishingChannelFactory.getPublishingChannels();

    }


    public void publishGraph(CayleyGraph cayleyGraph) throws ExternalResourceException {

        for (PublishingChannel publishingChannel : publishingChannels) {
            publishingChannel.publish(cayleyGraph);
        }

    }

}
