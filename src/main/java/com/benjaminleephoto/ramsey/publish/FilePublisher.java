package com.benjaminleephoto.ramsey.publish;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.benjaminleephoto.ramsey.common.CayleyGraph;
import com.benjaminleephoto.ramsey.common.Config;
import com.benjaminleephoto.ramsey.common.GraphFormatter;
import com.benjaminleephoto.ramsey.common.RamseyLogger;

public class FilePublisher implements PublishingChannel {

    private CayleyGraph cayleyGraph;
    private static final Logger logger = LoggerFactory.getLogger(FilePublisher.class.getName());


    public FilePublisher() {

        logger.info("Beginning FilePublisher initialization");
        logger.info("FilePublisher initialization complete");

    }


    public void publish(CayleyGraph cayleyGraph) {

        this.cayleyGraph = cayleyGraph;

        writeCheckpointFile();
        writeMaxFile();

        if (cayleyGraph.getCliqueCollection().getCliqueCount() == 0) {
            writeSolutionFile();
        }

    }


    private void writeSolutionFile() {
        writeGraphFile(Config.SOLUTION_FILE_PATH + Config.SOLUTION_FILE_MASK + RamseyLogger.getDateTimeStamp() + ".sol");
    }


    private void writeCheckpointFile() {
        writeGraphFile(Config.CHKPT_FILE_PATH + Config.CHKPT_FILE_MASK + RamseyLogger.getDateTimeStamp() + ".chk");
    }


    private void writeMaxFile() {
        writeGraphFile(Config.MAX_FILE_PATH + Config.MAX_FILE_MASK + "MAX" + ".chk");
    }


    /**
     * This is used to write a basic representation of an input CayleyGraph to a given file which
     * can later be loaded into this program.
     * 
     * @param cayleyGraph The CayleyGraph to be written to a file.
     * @param qualifiedFileName The file path and name where the CayleyGraph representation is to be
     *        written. 
     */
    private void writeGraphFile(String qualifiedFileName) {

        String content = GraphFormatter.getXMLString(cayleyGraph);
        File file = new File(qualifiedFileName);
        logger.info("Writing file: {}", qualifiedFileName);

        try (FileWriter fw = new FileWriter(file.getAbsoluteFile())) {

            fw.write(content);
        } catch (IOException e) {
            logger.error(content);
            logger.error("Error while writing file: {}", qualifiedFileName, e);
        }
    }
}
