package com.benjaminleephoto.ramsey.mutate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.benjaminleephoto.ramsey.common.ApplicationContext;
import com.benjaminleephoto.ramsey.common.CayleyGraph;
import com.benjaminleephoto.ramsey.common.Edge;
import com.benjaminleephoto.ramsey.common.EdgeColor;

public class GraphMutatorRandom implements GraphMutator {

    private CayleyGraph cayleyGraph;
    private static final Logger logger = LoggerFactory.getLogger(GraphMutatorRandom.class.getName());


    public GraphMutatorRandom() {
        logger.info("Initializing GraphMutatorRandom");
    }


    public void mutateGraph() {
        logger.info("Mutating Random");
        this.cayleyGraph = ApplicationContext.getCayleyGraph();
        mutateGraphRandom();
    }


    /**
     * Randomly flip one pair of edges, one edge of each color per pair to maintain balance.
     */
    private void mutateGraphRandom() {
        Edge redEdge = cayleyGraph.getRandomEdge(EdgeColor.RED);
        Edge blueEdge = cayleyGraph.getRandomEdge(EdgeColor.BLUE);

        redEdge.flipColor();
        blueEdge.flipColor();

        cayleyGraph.getCliqueCollection().clear();
    }

}
