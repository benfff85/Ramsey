package com.benjaminleephoto.ramsey.mutate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.benjaminleephoto.ramsey.common.ApplicationContext;
import com.benjaminleephoto.ramsey.common.CayleyGraph;
import com.benjaminleephoto.ramsey.common.Config;
import com.benjaminleephoto.ramsey.common.Edge;
import com.benjaminleephoto.ramsey.common.EdgeColor;

public class GraphMutatorComprehensive implements GraphMutator {

	private CayleyGraph cayleyGraph;
	private static final Logger logger = LoggerFactory.getLogger(GraphMutatorComprehensive.class.getName());

	public GraphMutatorComprehensive() {
		logger.info("Initializing GraphMutatorComprehensive");
	}

	public void mutateGraph() {
		logger.info("Mutating Comprehensive");
		this.cayleyGraph = ApplicationContext.getCayleyGraph();
		mutateGraphComprehensive();
	}

	/**
	 * This will flip the color of one edge from the identified clique along with
	 * one other edge of opposite color logically selected based off of color
	 * distribution.
	 * 
	 * @param clique
	 *            The clique to be mutated.
	 */
	private void mutateGraphComprehensive() {

		logger.debug("Beginning mutateGraphComprehensive method");

		Edge redEdge = cayleyGraph.getCliqueCollection().getMostCommonEdge(EdgeColor.RED,
				Config.MUTATE_COMPREHENSIVE_EDGE_RANGE);
		Edge blueEdge = cayleyGraph.getCliqueCollection().getMostCommonEdge(EdgeColor.BLUE,
				Config.MUTATE_COMPREHENSIVE_EDGE_RANGE);

		if (logger.isDebugEnabled()) {
			logger.debug("Red Edge is : {}", redEdge);
			logger.debug("Blue Edge is : {}", blueEdge);
		}

		logger.debug("Flipping edge colors");
		redEdge.flipColor();
		blueEdge.flipColor();

		logger.debug("Clearing clique collection post mutation.");
		cayleyGraph.getCliqueCollection().clear();

		logger.debug("Edges Flipped, exiting mutateGraphComprehensive");

	}
}
