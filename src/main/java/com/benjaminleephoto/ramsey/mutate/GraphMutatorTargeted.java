package com.benjaminleephoto.ramsey.mutate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.benjaminleephoto.ramsey.common.ApplicationContext;
import com.benjaminleephoto.ramsey.common.CayleyGraph;
import com.benjaminleephoto.ramsey.common.Config;
import com.benjaminleephoto.ramsey.common.Edge;
import com.benjaminleephoto.ramsey.common.EdgeColor;
import com.benjaminleephoto.ramsey.common.Vertex;

public class GraphMutatorTargeted implements GraphMutator {

    private CayleyGraph cayleyGraph;
    int vertexIdA;
    int vertexIdB;
    private static final Logger logger = LoggerFactory.getLogger(GraphMutatorTargeted.class.getName());


    public GraphMutatorTargeted() {
        logger.info("Initializing GraphMutatorTargeted");
    }


    public void mutateGraph() {
        logger.info("Mutating Targeted");
        this.cayleyGraph = ApplicationContext.getCayleyGraph();
        mutateGraphTargeted();
    }


    /**
     * This will flip the color of one random edge from the identified clique along with one random
     * edge of the opposite color to maintain balance.
     */
    private void mutateGraphTargeted() {

        logger.debug("Beginning mutateGraphTargeted method. Clique Color is {}", cayleyGraph.getClique().getColor());

        Vertex vertexA;
        Vertex vertexB;
        Edge cliqueEdge;
        Edge nonCliqueEdge;

        // Select the edge from clique to swap
        vertexIdA = 0;
        vertexIdB = 0;

        while (vertexIdA == vertexIdB) {
            vertexIdA = ApplicationContext.getGenerator().nextInt(Config.CLIQUE_SIZE);
            vertexIdB = ApplicationContext.getGenerator().nextInt(Config.CLIQUE_SIZE);
        }

        vertexA = cayleyGraph.getClique().getCliqueVertexByPosition(vertexIdA);
        vertexB = cayleyGraph.getClique().getCliqueVertexByPosition(vertexIdB);
        cliqueEdge = vertexA.getEdge(vertexB);

        if (cliqueEdge.getColor() == EdgeColor.RED) {
            nonCliqueEdge = cayleyGraph.getRandomEdge(EdgeColor.BLUE);
        } else {
            nonCliqueEdge = cayleyGraph.getRandomEdge(EdgeColor.RED);
        }

        cliqueEdge.flipColor();
        nonCliqueEdge.flipColor();

        cayleyGraph.getCliqueCollection().clear();

        logger.debug("Edges Flipped, exiting mutateGraphTargeted");

    }

}
