package com.benjaminleephoto.ramsey.mutate;

public interface GraphMutator {

    public void mutateGraph();

}
