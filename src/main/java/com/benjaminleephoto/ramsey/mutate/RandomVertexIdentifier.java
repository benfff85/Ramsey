package com.benjaminleephoto.ramsey.mutate;

import java.util.List;

import com.benjaminleephoto.ramsey.common.ApplicationContext;
import com.benjaminleephoto.ramsey.common.Vertex;

public class RandomVertexIdentifier {

    private RandomVertexIdentifier() {}


    public static VertexPair getRandomVertices(List<Vertex> vertexListA, List<Vertex> vertexListB) {
        Vertex vertexA;
        Vertex vertexB;

        vertexA = getRandomVertex(vertexListA);
        if (vertexListA.size() > 1) {
            vertexB = getRandomVertex(vertexListA);
        } else {
            vertexB = getRandomVertex(vertexListB);
        }

        return new VertexPair(vertexA, vertexB);
    }


    /**
     * This function uses a random number generator to return a random vertex from an input Vertex
     * ArrayList.
     * 
     * @param vertexList Vertex ArrayList from which a vertex will be returned at random.
     * @return A random Vertex from the input Vertex ArrayList.
     */
    public static Vertex getRandomVertex(List<Vertex> vertexList) {
        Vertex vertex;
        int index;

        index = ApplicationContext.getGenerator().nextInt(vertexList.size());
        vertex = vertexList.get(index);
        vertexList.remove(index);
        return vertex;

    }

}
