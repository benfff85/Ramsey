package com.benjaminleephoto.ramsey.mutate;

import com.benjaminleephoto.ramsey.common.Vertex;

public class VertexPair {

    private Vertex vertexA;
    private Vertex vertexB;


    VertexPair(Vertex vertexA, Vertex vertexB) {
        this.vertexA = vertexA;
        this.vertexB = vertexB;
    }


    public Vertex getVertexA() {
        return vertexA;
    }


    public void setVertexA(Vertex vertexA) {
        this.vertexA = vertexA;
    }


    public Vertex getVertexB() {
        return vertexB;
    }


    public void setVertexB(Vertex vertexB) {
        this.vertexB = vertexB;
    }
}
