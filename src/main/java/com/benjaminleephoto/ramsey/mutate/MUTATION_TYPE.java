package com.benjaminleephoto.ramsey.mutate;

public enum MUTATION_TYPE {
    RANDOM("R"), TARGETED("T"), BALANCED("B"), COMPREHENSIVE("C");
    private final String text;


    private MUTATION_TYPE(final String text) {
        this.text = text;
    }


    @Override
    public String toString() {
        return text;
    }
}
