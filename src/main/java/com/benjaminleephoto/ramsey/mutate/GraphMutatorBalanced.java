package com.benjaminleephoto.ramsey.mutate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.benjaminleephoto.ramsey.common.ApplicationContext;
import com.benjaminleephoto.ramsey.common.CayleyGraph;
import com.benjaminleephoto.ramsey.common.Clique;
import com.benjaminleephoto.ramsey.common.Edge;
import com.benjaminleephoto.ramsey.common.EdgeColor;

public class GraphMutatorBalanced implements GraphMutator {

	private CayleyGraph cayleyGraph;
	private static final Logger logger = LoggerFactory.getLogger(GraphMutatorBalanced.class.getName());

	public GraphMutatorBalanced() {
		logger.info("Initializing GraphMutatorBalanced");
	}

	public void mutateGraph() {
		logger.info("Mutating Balanced");
		this.cayleyGraph = ApplicationContext.getCayleyGraph();
		mutateGraphBalanced(cayleyGraph.getCliqueCollection().getRandomClique());
	}

	public void mutateGraphAll() {

		for (Clique clique : cayleyGraph.getCliqueCollection().getCliqueList()) {
			if (clique.isCliqueValid()) {
				mutateGraphBalanced(clique);
			}
		}
	}

	/**
	 * This will flip the color of one edge from the identified clique along with
	 * one other edge of opposite color logically selected based off of color
	 * distribution.
	 * 
	 * @param clique
	 *            The clique to be mutated.
	 */
	private void mutateGraphBalanced(Clique clique) {

		EdgeColor nonCliqueColor;

		logger.debug("Beginning mutateGraphBalanced method. Clique Color is {}", clique.getColor());

		if (clique.getColor() == EdgeColor.BLUE) {
			nonCliqueColor = EdgeColor.RED;
		} else {
			nonCliqueColor = EdgeColor.BLUE;
		}

		Edge cliqueEdge = EdgeRanker.getCliqueEdgeOfHighestRank(clique);
		Edge nonCliqueEdge = cayleyGraph.getRandomEdge(nonCliqueColor);

		if (logger.isDebugEnabled()) {
			logger.debug("Clique Edge is : {}", cliqueEdge);
			logger.debug("Non-Clique Edge is : {}", nonCliqueEdge);
		}

		logger.debug("Flipping edge colors");
		cliqueEdge.flipColor();
		nonCliqueEdge.flipColor();

		logger.debug("Clearing clique collection post mutation.");
		cayleyGraph.getCliqueCollection().clear();

		logger.debug("Edges Flipped, exiting mutateGraphBalanced");

	}
}
