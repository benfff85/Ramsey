package com.benjaminleephoto.ramsey.common;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MD5Hasher {

    private static final Logger logger = LoggerFactory.getLogger(MD5Hasher.class.getName());


    private MD5Hasher() {}


    public static String getMD5Hash(String message) {

        logger.info("Generating Hash");

        String output = DigestUtils.md5Hex(message);

        logger.info("Hash generated successfully");
        logger.info("HASH: {}", output);
        return output;

    }

}
