package com.benjaminleephoto.ramsey.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CumulativeStatistics {

    private int minCliqueCount;
    private long analyzedGraphCount;
    private CayleyGraph cayleyGraph;
    private static final Logger logger = LoggerFactory.getLogger(CumulativeStatistics.class.getName());


    /**
     * Main constructor for the CumulativeStatistics class.
     */
    public CumulativeStatistics() {
        analyzedGraphCount = 0;
        minCliqueCount = 999999999;
    }


    /**
     * This is the main publicly exposed method of this class which will trigger various updates
     * based on data currently found in the classes CayleyGraph object.
     */
    public void updateStatistics() {
        cayleyGraph = ApplicationContext.getCayleyGraph();
        incrementAnalyzedGraphCount();
        updateMinCliqueCount(cayleyGraph.getCliqueCollection());
    }


    /**
     * This will increment the number of analyzed graphs by one.
     */
    private void incrementAnalyzedGraphCount() {
        analyzedGraphCount += 1;
    }


    /**
     * This will check the number of cliques found for the current CayleyGraph. If the number of
     * cliques found is a new lowest value than the minCliqueCount values will be updated.
     * 
     * @param cliqueCollection Collection of cliques to be checked for a new minimum clique count.
     */
    public void updateMinCliqueCount(CliqueCollection cliqueCollection) {
        cayleyGraph = ApplicationContext.getCayleyGraph();
        int cliquecount = cliqueCollection.getCliqueCount();
        if (cliquecount < getMinCliqueCount()) {
            logger.debug("Updating minCliqueCount since {} is less then the already identified minCliqueCount of {}", cliquecount, getMinCliqueCount());
            minCliqueCount = cliquecount;
        } else {
            logger.debug("Not updating minCliqueCount since {} is not less then the already identified minCliqueCount of {}", cliquecount, getMinCliqueCount());
        }
    }


    /**
     * Standard getter for the analyzed graph count.
     * 
     * @return The number of graphs analyzed so far.
     */
    public long getAnalyzedGraphCount() {
        return analyzedGraphCount;
    }


    /**
     * Standard getter for the minimum clique count.
     * 
     * @return The minimum clique count seen so far.
     */
    public int getMinCliqueCount() {
        return minCliqueCount;
    }

}
