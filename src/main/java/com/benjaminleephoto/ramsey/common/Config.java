package com.benjaminleephoto.ramsey.common;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.benjaminleephoto.ramsey.mutate.MUTATION_TYPE;
import com.benjaminleephoto.ramsey.receive.RECEIVER_TYPE;

public class Config {

    private static final Logger logger = LoggerFactory.getLogger(Config.class.getName());

    public static final int NUM_OF_ELEMENTS;
    public static final int CLIQUE_SIZE;

    public static final RECEIVER_TYPE RECEIVER_METHOD = RECEIVER_TYPE.DATABASE;
    public static final LAUNCH_TYPE LAUNCH_METHOD = LAUNCH_TYPE.OPEN_MAX_FROM_DEFAULT_RECEIVER;

    public static final MUTATION_TYPE MUTATE_METHOD_PRIMARY = MUTATION_TYPE.BALANCED;
    public static final MUTATION_TYPE MUTATE_METHOD_SECONDARY = MUTATION_TYPE.BALANCED;
    public static final int MUTATE_COUNT;
    public static final int MUTATE_INTERVAL;
    public static final int MUTATE_COMPREHENSIVE_EDGE_RANGE;

    public static final int CLIQUE_SEARCH_THREAD_COUNT;

    public static final String CHKPT_FILE_PATH;
    public static final String CHKPT_FILE_MASK;
    public static final String MAX_FILE_PATH;
    public static final String MAX_FILE_MASK;
    public static final String SOLUTION_FILE_PATH;
    public static final String SOLUTION_FILE_MASK;

    public static final boolean PUBLISH_VIA_FILE;

    public static final String PUBLISH_DB_USER;
    public static final String PUBLISH_DB_PASS;
    public static final String PUBLISH_DB_ADDRESS;
    public static final int PUBLISH_DB_PORT;
    public static final boolean PUBLISH_VIA_DB;

    public static final int EDGE_COUNT;

    static {

        ConfigFileReader configFileReader = new ConfigFileReader();
        Properties properties = configFileReader.getConfigProperties();

        logger.debug("Setting CLIQUE_SIZE");
        CLIQUE_SIZE = Integer.parseInt(properties.getProperty("cliqueSize"));
        logger.info("CLIQUE_SIZE: {}", Config.CLIQUE_SIZE);

        logger.debug("Setting NUM_OF_ELEMENTS");
        NUM_OF_ELEMENTS = Integer.parseInt(properties.getProperty("numOfElements"));
        logger.info("NUM_OF_ELEMENTS: {}", Config.NUM_OF_ELEMENTS);

        logger.debug("Setting MUTATE_COUNT");
        MUTATE_COUNT = Integer.parseInt(properties.getProperty("mutateCount"));
        logger.info("MUTATE_COUNT: {}", Config.MUTATE_COUNT);

        logger.debug("Setting MUTATE_INTERVAL");
        MUTATE_INTERVAL = Integer.parseInt(properties.getProperty("mutateInterval"));
        logger.info("MUTATE_INTERVAL: {}", Config.MUTATE_INTERVAL);

        logger.debug("Setting MUTATE_COMPREHENSIVE_EDGE_RANGE");
        MUTATE_COMPREHENSIVE_EDGE_RANGE = Integer.parseInt(properties.getProperty("mutateComprehensiveEdgeRange"));
        logger.info("MUTATE_COMPREHENSIVE_EDGE_RANGE: {}", Config.MUTATE_COMPREHENSIVE_EDGE_RANGE);

        logger.debug("Setting CLIQUE_SEARCH_THREAD_COUNT");
        CLIQUE_SEARCH_THREAD_COUNT = Integer.parseInt(properties.getProperty("cliqueSearchThreadCount"));
        logger.info("CLIQUE_SEARCH_THREAD_COUNT: {}", Config.CLIQUE_SEARCH_THREAD_COUNT);

        logger.debug("Setting CHKPT_FILE_PATH");
        CHKPT_FILE_PATH = properties.getProperty("checkpointFilePath");
        logger.info("CHKPT_FILE_PATH: {}", Config.CHKPT_FILE_PATH);

        logger.debug("Setting CHKPT_FILE_MASK");
        CHKPT_FILE_MASK = properties.getProperty("checkpointFileMask");
        logger.info("CHKPT_FILE_MASK: {}", Config.CHKPT_FILE_MASK);

        logger.debug("Setting MAX_FILE_PATH");
        MAX_FILE_PATH = properties.getProperty("maxFilePath");
        logger.info("MAX_FILE_PATH: {}", Config.MAX_FILE_PATH);

        logger.debug("Setting MAX_FILE_MASK");
        MAX_FILE_MASK = properties.getProperty("maxFileMask");
        logger.info("MAX_FILE_MASK: {}", Config.MAX_FILE_MASK);

        logger.debug("Setting SOLUTION_FILE_PATH");
        SOLUTION_FILE_PATH = properties.getProperty("solutionFilePath");
        logger.info("SOLUTION_FILE_PATH: {}", Config.SOLUTION_FILE_PATH);

        logger.debug("Setting SOLUTION_FILE_MASK");
        SOLUTION_FILE_MASK = properties.getProperty("solutionFileMask");
        logger.info("SOLUTION_FILE_MASK: {}", Config.SOLUTION_FILE_MASK);

        logger.debug("Setting PUBLISH_VIA_FILE");
        PUBLISH_VIA_FILE = Boolean.parseBoolean(properties.getProperty("publishViaFile"));
        logger.info("PUBLISH_VIA_FILE: {}", Config.PUBLISH_VIA_FILE);

        logger.debug("Setting PUBLISH_DB_USER");
        PUBLISH_DB_USER = properties.getProperty("databaseUser");
        logger.info("PUBLISH_DB_USER: {}", Config.PUBLISH_DB_USER);

        logger.debug("Setting PUBLISH_DB_PASS");
        PUBLISH_DB_PASS = properties.getProperty("databasePass");
        logger.info("PUBLISH_DB_PASS: {}", Config.PUBLISH_DB_PASS);

        logger.debug("Setting PUBLISH_DB_ADDRESS");
        PUBLISH_DB_ADDRESS = properties.getProperty("databaseAddress");
        logger.info("PUBLISH_DB_ADDRESS: {}", Config.PUBLISH_DB_ADDRESS);

        logger.debug("Setting PUBLISH_DB_PORT");
        PUBLISH_DB_PORT = Integer.parseInt(properties.getProperty("databasePort"));
        logger.info("PUBLISH_DB_PORT: {}", Config.PUBLISH_DB_PORT);

        logger.debug("Setting PUBLISH_VIA_DB");
        PUBLISH_VIA_DB = Boolean.parseBoolean(properties.getProperty("publishViaDB"));
        logger.info("PUBLISH_VIA_DB: {}", Config.PUBLISH_VIA_DB);

        EDGE_COUNT = ((Config.NUM_OF_ELEMENTS) * (Config.NUM_OF_ELEMENTS - 1)) / 2;

    }


    private Config() {}

}
