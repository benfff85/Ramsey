package com.benjaminleephoto.ramsey.common;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.benjaminleephoto.ramsey.cliquechecker.CliqueCheckerFactory;
import com.benjaminleephoto.ramsey.mutate.GraphMutatorFactory;
import com.benjaminleephoto.ramsey.publish.Publisher;
import com.benjaminleephoto.ramsey.receive.Receiver;

/**
 * This is the main Controller Class where the program will initiate. It handles the outermost loop
 * of logic which encompasses a framework for the entire application.
 * 
 * @author Ben Ferenchak
 * @version 1.0
 */
public class Controller {

    private RamseyLogger ramseyLogger;
    private Timer timer;
    private CumulativeStatistics stats;
    private boolean counterExampleFound;
    
    private static final String MUTATE_TIMER_NAME = "MUTATE";
    private static final String LOGGER_TIMER_NAME = "LOGGER";
    private static final String CLIQUE_TIMER_NAME = "CLIQUE";
    private static final String STATS_TIMER_NAME = "STATS";

    private static final Logger logger = LoggerFactory.getLogger(Controller.class.getName());


    /**
     * This is the main method for the application. It will initialize the CayleyGraph object and
     * begin searching for a counter example. A counter example is defined as a CayleyGraph with no
     * clique (complete subgraph) within it. This method will loop continually searching and
     * mutating until a counter example is found.
     * <p>
     * It is noted the passing of Strings to methods as done below for the purposes of switch
     * statements is generally poor practice, however it has done here to allow for quick
     * modifications and testing of the code without the use of a GUI.
     * 
     * @throws Exception
     */
    public Controller() throws ExternalResourceException {

        logger.info("Beginning Controller initialization.");

        ApplicationContext.setGenerator(new Random());
        ApplicationContext.setPublisher(new Publisher());
        ApplicationContext.setReceiver(new Receiver());
        ApplicationContext.setCliqueCheckerFactory(new CliqueCheckerFactory());
        ApplicationContext.setGraphMutatorFactory(new GraphMutatorFactory());
        ApplicationContext.setCheckpointManager(new CheckpointManager());
        ApplicationContext.setCayleyGraph(new CayleyGraph());

        timer = new Timer();
        stats = new CumulativeStatistics();
        ramseyLogger = new RamseyLogger(ApplicationContext.getCayleyGraph(), timer, stats);

        counterExampleFound = false;

        timer.newTimeSet(MUTATE_TIMER_NAME);
        timer.newTimeSet(LOGGER_TIMER_NAME);
        timer.newTimeSet(CLIQUE_TIMER_NAME);
        timer.newTimeSet(STATS_TIMER_NAME);

        logger.info("Logger initialization successful.");
    }


    public void runSearch() throws ExternalResourceException {
        logger.info("Beginning search...");
        while (!counterExampleFound) {
            runIteration();
        }
    }


    public void runIteration() throws ExternalResourceException {

        buildCliqueCollection();

        // Make sure what we received isn't actually a solution
        if (!ApplicationContext.getCayleyGraph().isCliqueIdentified()) {
            counterExampleFound = true;
            ramseyLogger.processPositiveCase(ApplicationContext.getCayleyGraph());
            return;
        }

        processMutation();

        findCliques();

        processStatistics();

        processLogging();

        if (!ApplicationContext.getCayleyGraph().isCliqueIdentified()) {
            counterExampleFound = true;
            ramseyLogger.processPositiveCase(ApplicationContext.getCayleyGraph());
            return;
        } else {
            ramseyLogger.processCheckpoint();
        }

        ApplicationContext.getCayleyGraph().loadFromString(ApplicationContext.getReceiver().receiveGraph());

    }


    private void buildCliqueCollection() {
        ApplicationContext.getCheckpointManager().buildCliqueCollection();
        stats.updateMinCliqueCount(ApplicationContext.getCayleyGraph().getCliqueCollection());
    }


    private void findCliques() {
        timer.startTimer(CLIQUE_TIMER_NAME);
        ApplicationContext.getCliqueCheckerFactory().getCliqueChecker().findClique(EdgeColor.RED);
        ApplicationContext.getCliqueCheckerFactory().getCliqueChecker().findClique(EdgeColor.BLUE);
        timer.endTimer(CLIQUE_TIMER_NAME);
    }


    private void processLogging() {
        timer.startTimer(LOGGER_TIMER_NAME);
        ramseyLogger.logIteration();
        timer.endTimer(LOGGER_TIMER_NAME);
    }


    private void processMutation() {
        timer.startTimer(MUTATE_TIMER_NAME);
        ApplicationContext.getGraphMutatorFactory().getGraphMutator().mutateGraph();
        timer.endTimer(MUTATE_TIMER_NAME);
    }


    private void processStatistics() {
        timer.startTimer(STATS_TIMER_NAME);
        stats.updateStatistics();
        timer.endTimer(STATS_TIMER_NAME);
    }

}
