package com.benjaminleephoto.ramsey.common;

public class ExternalResourceException extends Exception {

	private static final long serialVersionUID = 1L;

	public ExternalResourceException(String message) {
		super(message);
	}
}
