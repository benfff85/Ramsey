package com.benjaminleephoto.ramsey.common;

public enum LAUNCH_TYPE {
    OPEN_MAX_FROM_DEFAULT_RECEIVER("R"), OPEN_MAX_FROM_FILE("F"), OPEN_MAX_FROM_DATABASE("D"), GENERATE_RANDOM("R"), OPEN_FROM_FILE_INTERACTIVE("I");
    private final String text;


    private LAUNCH_TYPE(final String text) {
        this.text = text;
    }


    @Override
    public String toString() {
        return text;
    }
}
