package com.benjaminleephoto.ramsey.common;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.XMLOutputter;

public class GraphFormatter {

    private GraphFormatter() {}


    public static String getXMLString(CayleyGraph cayleyGraph) {

        Element root = new Element("RAMSEY");
        Document doc = new Document();

        Element child1 = new Element("EDGES");
        child1.addContent(getCondensedGraphData(cayleyGraph));
        Element child2 = new Element("DATE");
        child2.addContent(RamseyLogger.getDateTimeStamp());
        Element child3 = new Element("CLIQUES");
        child3.addContent(Integer.toString(cayleyGraph.getCliqueCollection().getCliqueCount()));

        root.addContent(child1);
        root.addContent(child2);
        root.addContent(child3);

        doc.setRootElement(root);

        XMLOutputter outputter = new XMLOutputter();

        return outputter.outputString(doc);

    }


    /**
     * This will generate and return a String representing the CayleyGraph in a basic common
     * separated value format.
     * 
     * @return String representing the CayleyGraph in a basic common separated value format.
     */
    public static String getCayleyGraphBasic(CayleyGraph cayleyGraph) {
        StringBuilder output = new StringBuilder();
        int numOfElements = cayleyGraph.getNumOfElements();

        for (int i = 0; i < numOfElements; i++) {
            for (int j = 0; j < numOfElements; j++) {

                if (i != j && cayleyGraph.getEdgeByVertexIds(i, j).getColor() == EdgeColor.RED) {
                    output.append("1,");
                } else {
                    output.append("0,");
                }
            }
            output.setLength(output.length() - 1);
            output.append("\n");
        }
        return output.toString();
    }


    /**
     * This will generate and return a String representing the CayleyGraph in a format compatible
     * with Mathematica.
     * 
     * @return String representing the CayleyGraph in a format compatible with Mathematica.
     */
    public static String getCayleyGraphMathematica(CayleyGraph cayleyGraph) {
        StringBuilder output = new StringBuilder();
        int numOfElements = cayleyGraph.getNumOfElements();
        output.append("GraphPlot[{");

        for (int i = 0; i < numOfElements; i++) {
            output.append("{");
            for (int j = 0; j < numOfElements; j++) {

                if (i != j && cayleyGraph.getEdgeByVertexIds(i, j).getColor() == EdgeColor.RED) {
                    output.append("1, ");
                } else {
                    output.append("0, ");
                }
            }
            output.setLength(output.length() - 2);
            output.append("},");
        }
        output.setLength(output.length() - 1);
        output.append("}, Method -> CircularEmbedding]");
        return output.toString();
    }


    /**
     * This will generate a String with condensed graph data. It is simply a String with numeric
     * values for every edge in the graph. As usual BLUE=0 and RED=1.
     * 
     * @return A String representing the colors of all edges in a given graph.
     */
    public static String getCondensedGraphData(CayleyGraph cayleyGraph) {
        StringBuilder output = new StringBuilder();
        int numOfElements = cayleyGraph.getNumOfElements();

        for (int i = 0; i < numOfElements; i++) {
            for (int j = i + 1; j < numOfElements; j++) {
                if (cayleyGraph.getEdgeByVertexIds(i, j).getColor() == EdgeColor.BLUE) {
                    output.append("0");
                } else {
                    output.append("1");
                }
            }
        }

        return output.toString();

    }

}
