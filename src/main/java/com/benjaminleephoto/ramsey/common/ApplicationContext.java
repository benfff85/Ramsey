package com.benjaminleephoto.ramsey.common;

import java.util.Random;

import com.benjaminleephoto.ramsey.cliquechecker.CliqueCheckerFactory;
import com.benjaminleephoto.ramsey.mutate.GraphMutatorFactory;
import com.benjaminleephoto.ramsey.publish.Publisher;
import com.benjaminleephoto.ramsey.receive.Receiver;

public class ApplicationContext {

    private static CayleyGraph cayleyGraph;
    private static Random generator;
    private static Publisher publisher;
    private static Receiver receiver;
    private static CliqueCheckerFactory cliqueCheckerFactory;
    private static GraphMutatorFactory graphMutatorFactory;
    private static CheckpointManager checkpointManager;


    private ApplicationContext() {}


    public static CayleyGraph getCayleyGraph() {
        return cayleyGraph;
    }


    public static void setCayleyGraph(CayleyGraph cayleyGraph) {
        ApplicationContext.cayleyGraph = cayleyGraph;
    }


    public static Random getGenerator() {
        return generator;
    }


    public static void setGenerator(Random generator) {
        ApplicationContext.generator = generator;
    }


    public static Publisher getPublisher() {
        return publisher;
    }


    public static void setPublisher(Publisher publisher) {
        ApplicationContext.publisher = publisher;
    }


    public static Receiver getReceiver() {
        return receiver;
    }


    public static void setReceiver(Receiver receiver) {
        ApplicationContext.receiver = receiver;
    }


    public static CliqueCheckerFactory getCliqueCheckerFactory() {
        return cliqueCheckerFactory;
    }


    public static void setCliqueCheckerFactory(CliqueCheckerFactory cliqueCheckerFactory) {
        ApplicationContext.cliqueCheckerFactory = cliqueCheckerFactory;
    }


    public static GraphMutatorFactory getGraphMutatorFactory() {
        return graphMutatorFactory;
    }


    public static void setGraphMutatorFactory(GraphMutatorFactory graphMutatorFactory) {
        ApplicationContext.graphMutatorFactory = graphMutatorFactory;
    }


    public static CheckpointManager getCheckpointManager() {
        return checkpointManager;
    }


    public static void setCheckpointManager(CheckpointManager checkpointManager) {
        ApplicationContext.checkpointManager = checkpointManager;
    }

}
