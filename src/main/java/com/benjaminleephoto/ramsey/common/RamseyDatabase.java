package com.benjaminleephoto.ramsey.common;

import java.sql.Connection;
import java.sql.SQLException;

import com.mysql.cj.jdbc.MysqlDataSource;

public class RamseyDatabase {

    private static Connection connection = null;


    private RamseyDatabase() {}


    public static Connection getConnection() throws ExternalResourceException {
        if (connection == null) {
            MysqlDataSource dataSource = new MysqlDataSource();
            dataSource.setUser(Config.PUBLISH_DB_USER);
            dataSource.setPassword(Config.PUBLISH_DB_PASS);
            dataSource.setServerName(Config.PUBLISH_DB_ADDRESS);
            dataSource.setPort(Config.PUBLISH_DB_PORT);
            try {
				connection = dataSource.getConnection();
			} catch (SQLException e) {
				throw new ExternalResourceException(e.getMessage());
			}
        }
        return connection;
    }

}
