package com.benjaminleephoto.ramsey.common;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigFileReader {

    private static final Logger logger = LoggerFactory.getLogger(ConfigFileReader.class.getName());


    public Properties getConfigProperties() {

        Properties properties = new Properties();
        String propFileName = "config.properties";
        logger.info("Attempting to load configuration from {}", propFileName);

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName)) {

            if (inputStream == null) {
                throw new FileNotFoundException("Property file '" + propFileName + "' not found in the classpath");
            }

            properties.load(inputStream);

            logger.debug("Configuration loaded to properties object");

        } catch (Exception e) {
            logger.error("Unable to load file, need to determine how best to handle this because we cant throw an exception to the static initialization block");
            logger.error("Exception: ", e);

        }

        return properties;
    }

}
