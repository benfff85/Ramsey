package com.benjaminleephoto.ramsey.common;

public class Launch {

    private Launch() {}


    public static void main(String[] args) throws Exception {
        Controller controller = new Controller();
        controller.runSearch();
    }
}
