package com.benjaminleephoto.ramsey.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CheckpointManager {

    private String checkpointGraphHash;
    private CliqueCollectionSnapshot cliqueCollectionSnapshot;
    private CayleyGraph cayleyGraph;
    private static final Logger logger = LoggerFactory.getLogger(CheckpointManager.class.getName());


    public CheckpointManager() {
        checkpointGraphHash = null;
        cliqueCollectionSnapshot = new CliqueCollectionSnapshot();
    }


    public void buildCliqueCollection() {

        logger.debug("Starting buildCliqueCollection");
        cayleyGraph = ApplicationContext.getCayleyGraph();

        cayleyGraph.getCliqueCollection().clear();

        if (checkpointGraphHash != null && checkpointGraphHash.equals(MD5Hasher.getMD5Hash(GraphFormatter.getCondensedGraphData(cayleyGraph)))) {
            buildCliqueCollectionFromSnapshot();
        } else {
            buildCliqueCollectionFromScratch();
        }

        logger.debug("buildCliqueCollection complete");

    }


    private void buildCliqueCollectionFromSnapshot() {
        Vertex[] vertices;
        int cliqueSize;
        int[] cliqueDataToRecover;

        logger.debug("Recovering CliqueCollection from snapshot");
        for (int i = 0; i < cliqueCollectionSnapshot.getCliqueCount(); i++) {
            cliqueDataToRecover = cliqueCollectionSnapshot.getCliqueByPosition(i);
            cliqueSize = cliqueDataToRecover.length;
            vertices = new Vertex[cliqueSize];
            for (int j = 0; j < cliqueSize; j++) {
                vertices[j] = cayleyGraph.getVertexById(cliqueDataToRecover[j]);
            }

            cayleyGraph.getCliqueCollection().addClique(new Clique(vertices));

        }
    }


    private void buildCliqueCollectionFromScratch() {
        logger.debug("Hash of received graph does not match, identify cliques");
        ApplicationContext.getCliqueCheckerFactory().getCliqueChecker().findClique(EdgeColor.RED);
        ApplicationContext.getCliqueCheckerFactory().getCliqueChecker().findClique(EdgeColor.BLUE);

        updateShapshot();
    }


    public void updateShapshot() {
        logger.debug("Updating snapshot graph hash");
        checkpointGraphHash = MD5Hasher.getMD5Hash(GraphFormatter.getCondensedGraphData(cayleyGraph));
        logger.debug("Updating clique collection snapshot");
        cliqueCollectionSnapshot.clearCliqueCollectionArray();
        cliqueCollectionSnapshot.populateSnapshot(cayleyGraph.getCliqueCollection());
    }

}
