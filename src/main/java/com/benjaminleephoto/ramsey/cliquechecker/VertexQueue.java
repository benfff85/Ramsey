package com.benjaminleephoto.ramsey.cliquechecker;

public class VertexQueue {

    private static int vertexId;


    private VertexQueue() {}


    public static synchronized int getVertexId() {
        return vertexId++;
    }


    public static synchronized void resetVertexId() {
        vertexId = 0;
    }

}
