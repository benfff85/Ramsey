package com.benjaminleephoto.ramsey.cliquechecker;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.benjaminleephoto.ramsey.common.CayleyGraph;
import com.benjaminleephoto.ramsey.common.Clique;
import com.benjaminleephoto.ramsey.common.Config;
import com.benjaminleephoto.ramsey.common.EdgeColor;
import com.benjaminleephoto.ramsey.common.Vertex;

class CliqueCheckerThread implements Callable<Integer> {

    private int threadId;
    private static CayleyGraph cayleyGraph;
    private static EdgeColor color;
    private static final Logger logger = LoggerFactory.getLogger(CliqueCheckerThread.class.getName());


    CliqueCheckerThread(int threadId) {
        this.threadId = threadId;
    }


    @Override
    public Integer call() {
        logger.trace("Starting FindCliqueThread {} ", threadId);
        int i = VertexQueue.getVertexId();
        while (i < cayleyGraph.getNumOfElements() - Config.CLIQUE_SIZE) {

            try {
                findCliqueRecursive(i, color);
            } catch (Exception e) {
                logger.error("Error during clique checking", e);
            }
            logger.trace("Finished checking vertex {} for cliques", i);
            i = VertexQueue.getVertexId();

        }
        logger.trace("Finished FindCliqueThread {}", threadId);
        return null;

    }


    private void findCliqueRecursive(int startingVertexID, EdgeColor color) throws Exception {
        ArrayList<Vertex> connectedVertices = new ArrayList<>();
        connectedVertices.add(cayleyGraph.getVertexById(startingVertexID));
        findCliqueRecursive(connectedVertices, color);
        connectedVertices.clear();
    }


    private void findCliqueRecursive(ArrayList<Vertex> connectedVertices, EdgeColor color) throws Exception {

        // Loop through all vertices starting with the one after the last vertex in the chain
        for (int i = connectedVertices.get(connectedVertices.size() - 1).getId() + 1; i < cayleyGraph.getNumOfElements(); i++) {
            // If the vertex being considered is connected
            if (isConnected(connectedVertices, cayleyGraph.getVertexById(i), color)) {
                connectedVertices.add(cayleyGraph.getVertexById(i));
                // If this and makes a completed clique add it to the clique collection
                if (connectedVertices.size() == Config.CLIQUE_SIZE) {
                    cayleyGraph.getCliqueCollection().addClique(new Clique(connectedVertices));
                }
                // Otherwise if there are enough possible options left to form a clique proceed with
                // search. Optimize by adding second condition above.
                else {
                    findCliqueRecursive(connectedVertices, color);
                }
                // Remove this vertex from the chain and try the next at this level
                connectedVertices.remove(cayleyGraph.getVertexById(i));
            }
        }

        // Once all have been tried at this level return
        return;
    }


    private static boolean isConnected(ArrayList<Vertex> connectedVertices, Vertex vertex, EdgeColor color) {
        for (int i = 0; i < connectedVertices.size(); i++) {
            if (connectedVertices.get(i).getEdge(vertex).getColor() != color) {
                return false;
            }
        }
        return true;
    }


    public static CayleyGraph getCayleyGraph() {
        return cayleyGraph;
    }


    public static void setCayleyGraph(CayleyGraph cayleyGraph) {
        CliqueCheckerThread.cayleyGraph = cayleyGraph;
    }


    public static EdgeColor getColor() {
        return color;
    }


    public static void setColor(EdgeColor color) {
        CliqueCheckerThread.color = color;
    }

}
