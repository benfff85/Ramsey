package com.benjaminleephoto.ramsey.cliquechecker;

import com.benjaminleephoto.ramsey.common.EdgeColor;

public interface CliqueChecker {

    public void findClique(EdgeColor color);

}
