pipeline {
    agent { label 'docker' }
    options {
        buildDiscarder(logRotator(numToKeepStr: '5'))
    }
    tools {
        maven 'Maven'
        jdk 'Java 10'
    }
    stages {
        stage ('Initialize') {
            steps {
                sh '''
                    echo "PATH = ${PATH}"
                    echo "M2_HOME = ${M2_HOME}"
                '''
            }
        }

        stage('SonarQube') {
            steps {
                script {
                    withSonarQubeEnv('SonarQube') {
                        sh 'mvn clean sonar:sonar' 
                    }
                }
            }
        }

        stage ('Maven Compile and Package') {
            steps {
                sh 'mvn clean package'
            }
        }

        stage ('Build Project Structure') {
            steps {
                sh 'rm -rf Ramsey'
                sh 'mkdir Ramsey'
                sh 'cp -r ./src/main/cfg Ramsey'
                sh 'cp -r ./src/main/resources Ramsey'
                sh 'cp -r ./src/main/max Ramsey'
                sh 'mkdir Ramsey/lib'
                sh 'cp ./target/Ramsey-*.jar Ramsey/lib/'
            }
        }

        stage ('Tar') {
            steps {
                sh 'touch ramsey.tar'
                sh 'rm ramsey.tar'
                sh 'tar -cvf ramsey.tar Ramsey'
                sh 'ls -ltr ramsey.tar'
            }
        }

        stage ('GZip') {
            steps {
                sh 'touch ramsey.tar.gz'
                sh 'rm ramsey.tar.gz'
                sh 'gzip -9 ramsey.tar'
                sh 'ls -ltr ramsey.tar.gz'
            }
        }

        stage ('Docker') {
            steps {
                sh 'find Ramsey -ls'
                sh 'docker build -t benferenchak/ramsey-private .'
                withDockerRegistry([ credentialsId: "docker-hub-credentials", url: "" ]) {
                    sh 'docker push benferenchak/ramsey-private:latest'
                }
            }
        }

    }
    post {
        success {
            mail to: 'ben.ferenchak@gmail.com',
            subject: "Successful Pipeline: ${currentBuild.fullDisplayName}",
            body: "Successful build completed ${env.BUILD_URL}"
        }
        failure {
            mail to: 'ben.ferenchak@gmail.com',
            subject: "Failed Pipeline: ${currentBuild.fullDisplayName}",
            body: "Something is wrong with ${env.BUILD_URL}"
        }
    }
}