FROM openjdk:10-jre-slim
COPY ramsey.tar.gz /
RUN gunzip /ramsey.tar.gz
RUN tar -xvf /ramsey.tar
RUN rm /ramsey.tar
CMD ["java", "-cp", "/Ramsey/cfg:/Ramsey/resources/log4j2.xml:/Ramsey/lib/Ramsey-1.0.0-SNAPSHOT.jar", "com.benjaminleephoto.ramsey.common.Launch"]
